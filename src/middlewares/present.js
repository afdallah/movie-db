module.exports = (req, res) => {
  let statusCode = req.body.statusCode
  if (!statusCode) return res.status(404).json({
    status: false,
    statusCode: 404,
    errors: 'Not found!'
  })

  res.status(statusCode).json(req.body)
}
