const jwt = require('jsonwebtoken')

module.exports = {
  token: async (req, res, next) => {
    let token = req.headers.authorization;

    let response = {
      status: false,
      statusCode: 401
    }
    
    if (!token.includes("Bearer"))
      return res.status(401).json({
        ...response,
        errors: "Token should include bearer prefix"
      });

    try {
      token = token.split('Bearer ')[1];
      let payload = await jwt.verify(token, process.env.JWT_SIGNATURE_KEY);
      req.headers.authorization = payload;
      return next()
    }

    catch(_) {
      return res.status(401).json({
        ...response,
        errors: "Invalid token"
      })
    }
  },

  isVerified: async (req, res, next) => {
    if (req.headers.authorization.isVerified) return next()

    return res.status(405).json({
      status: false,
      statusCode: 405,
      errors: "You need to verify your account to do this action"
    }) 
  }
}
