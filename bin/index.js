const app = require('../src/index.js')
const port = 8000 || process.env.PORT

app.listen(port, () => {
  console.log(`Server started at ${Date()}`)
  console.log(`Listening on port ${port}!`)
})
