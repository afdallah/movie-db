const server = require('../index.spec.js')
const request = require('supertest')
const User = require('../../src/models/user.js')
const jwt = require('jsonwebtoken')
const fixture = require('../fixtures/user.js')
const faker = require('faker')

describe('Auth Collection', () => {

  describe('POST /api/v1/auth/register', () => {
    let init = fixture()
    
    beforeAll(done => {
      User.create(init)
        .then(() => done())
    })

    afterAll(done => {
      User.remove()
        .then(() => done())
    })
    
    test('Should successfully register new user', done => {
      let data = JSON.stringify(
        fixture()
      )

      request(server)
        .post('/api/v1/auth/register')
        .set('Content-Type', 'application/json')
        .send(data)
        .expect(200)
        .end((_, res) => {
          expect(res.body.status).toEqual(true)
          expect(res.body.data).toHaveProperty('_id')
          expect(res.body.data).toHaveProperty('username')
          expect(res.body.data).toHaveProperty('email')
          expect(res.body.data).toHaveProperty('isVerified')
          done()
        })
    })

    test('Failed to register because of duplication', done => {
      let data = JSON.stringify(init)

      request(server)
        .post('/api/v1/auth/register')
        .set('Content-Type', 'application/json')
        .send(data)
        .expect(400)
        .end((_, res) => {
          expect(res.body.status).toEqual(false)
          expect(res.body).toHaveProperty('errors')
          done()
        })
    })

    test('Failed to register because of incomplete property', done => {
      let data = {
        ...init
      } 

      delete data.email;
      data = JSON.stringify(data)

      request(server)
        .post('/api/v1/auth/register')
        .set('Content-Type', 'application/json')
        .send(data)
        .expect(400)
        .end((_, res) => {
          expect(res.body.status).toEqual(false)
          expect(res.body).toHaveProperty('errors')
          expect(res.body.errors).toHaveProperty('email')
          expect(res.body.errors.email).toBe('email should have been string')

          done()
        })
    })

    test("Failed to register because of password doesn't match", done => {
      let data = {
        ...init
      } 

      data.password_confirmation = faker.random.uuid()
      data = JSON.stringify(data)

      request(server)
        .post('/api/v1/auth/register')
        .set('Content-Type', 'application/json')
        .send(data)
        .expect(400)
        .end((_, res) => {
          expect(res.body.status).toEqual(false)
          expect(res.body).toHaveProperty('errors')
          expect(res.body.errors).toBe("Password and its confirmation doesn't match")

          done()
        })
    })
  })

  describe('POST /api/v1/auth/login', () => {
    let init = fixture()
    
    beforeAll(done => {
      User.create(init)
        .then(() => {
          delete init.password_confirmation
          done()
        })
    })

    afterAll(done => {
      User.remove()
        .then(() => done())
    })
    
    test('Should successfully login by email and get token', done => {
      let data = {
        ...init
      }
      delete data.username
      data = JSON.stringify(data)

      request(server)
        .post('/api/v1/auth/login')
        .set('Content-Type', 'application/json')
        .send(data)
        .expect(200)
        .end((_, res) => {
          expect(res.body.status).toEqual(true)
          expect(res.body.data).toHaveProperty('_id')
          expect(res.body.data).toHaveProperty('username')
          expect(res.body.data).toHaveProperty('email')
          expect(res.body.data).toHaveProperty('isVerified')
          expect(res.body.data).toHaveProperty('token')
          done()
        })
    })

    test('Should successfully login by username and get token', done => {
      let data = {
        ...init
      }
      delete data.email
      data = JSON.stringify(data)

      request(server)
        .post('/api/v1/auth/login')
        .set('Content-Type', 'application/json')
        .send(data)
        .expect(200)
        .end((_, res) => {
          expect(res.body.status).toEqual(true)
          expect(res.body.data).toHaveProperty('_id')
          expect(res.body.data).toHaveProperty('username')
          expect(res.body.data).toHaveProperty('email')
          expect(res.body.data).toHaveProperty('isVerified')
          expect(res.body.data).toHaveProperty('token')
          done()
        })
    })

    test('Failed to login because of incomplete request body', done => {
      let data = {
        ...init
      } 

      delete data.email;
      delete data.username;
      data = JSON.stringify(data)

      request(server)
        .post('/api/v1/auth/login')
        .set('Content-Type', 'application/json')
        .send(data)
        .expect(400)
        .end((_, res) => {
          expect(res.body.status).toEqual(false)
          expect(res.body).toHaveProperty('errors')
          expect(res.body.errors).toBe('Username or email is required')

          done()
        })
    })

    test('Failed to login because of no password were sent', done => {
      let data = {
        ...init
      } 

      delete data.password
      data = JSON.stringify(data)

      request(server)
        .post('/api/v1/auth/login')
        .set('Content-Type', 'application/json')
        .send(data)
        .expect(400)
        .end((_, res) => {
          expect(res.body.status).toEqual(false)
          expect(res.body).toHaveProperty('errors')
          expect(res.body.errors).toBe('Password is required')

          done()
        })
    })

    test('Failed to login because of wrong password', done => {
      let data = {
        ...init
      } 
      data.password = fixture().password
      data = JSON.stringify(data)

      request(server)
        .post('/api/v1/auth/login')
        .set('Content-Type', 'application/json')
        .send(data)
        .expect(400)
        .end((_, res) => {
          expect(res.body.status).toEqual(false)
          expect(res.body).toHaveProperty('errors')
          expect(res.body.errors).toBe('Incorrect password!')

          done()
        })
    })

    test("Failed to login because user doesn't exist", done => {
      let data = fixture() 
      data = JSON.stringify(data)

      request(server)
        .post('/api/v1/auth/login')
        .set('Content-Type', 'application/json')
        .send(data)
        .expect(400)
        .end((_, res) => {
          expect(res.body.status).toEqual(false)
          expect(res.body).toHaveProperty('errors')
          expect(res.body.errors).toBe("User doesn't seems to be existed!")

          done()
        })
    })
  })

  describe('GET /api/v1/auth/verify', () => {
    let init = fixture()
    
    beforeAll(done => {
      User.create(init)
        .then(() => {
          delete init.password_confirmation
          done()
        })
    })

    afterAll(done => {
      User.remove()
        .then(() => done())
    })
    
    test('Should successfully verify account', done => {
      User.findOne({ email: init.email })
        .then(data => {
          let { _id } = data
          let token = jwt.sign({ _id }, process.env.VERIFICATION_SECRET)

          request(server)
            .get('/api/v1/auth/verify?verification_token=' + token)
            .expect(301)
            .end((err, res) => {
              expect(res.text.includes("success=true")).toEqual(true)
              expect(res.text.includes(init.username.toLowerCase())).toEqual(true)
              expect(res.text.includes("%20is%20now%20verified")).toEqual(true)
              done()
            })
        })
    })

    test("Won't verify the same account that is already verified", done => {
      User.findOne({ email: init.email })
        .then(data => {
          let { _id } = data
          let token = jwt.sign({ _id }, process.env.VERIFICATION_SECRET)

          request(server)
            .get('/api/v1/auth/verify?verification_token=' + token)
            .expect(301)
            .end((err, res) => {
              expect(res.text.includes("success=true")).toEqual(true)
              expect(res.text.includes(init.username.toLowerCase())).toEqual(true)
              expect(res.text.includes("%20is%20already%20verified")).toEqual(true)
              done()
            })
        })
    })

    test("Won't verify because of blindly hitting this endpoint without sending verification_token", done => {
      request(server)
        .get('/api/v1/auth/verify?verification_token=')
        .expect(307)
        .end((err, res) => {
          expect(res.text.includes("success=false")).toEqual(true)
          expect(res.text.includes("message=jwt%20must%20be%20provided")).toEqual(true)
          done()
        })
    })

    test("Won't verify because of mismatch secret key", done => {
      User.findOne({ email: init.email })
        .then(data => {
          let { _id } = data
          let token = jwt.sign({ _id }, process.env.JWT_SIGNATURE_KEY)

          request(server)
            .get('/api/v1/auth/verify?verification_token=' + token)
            .expect(307)
            .end((err, res) => {
              expect(res.text.includes("success=false")).toEqual(true)
              expect(res.text.includes("message=invalid%20signature")).toEqual(true)
              done()
            })
        })
    })

    test("Rare case: Valid verification token, but user doesn't exist", done => {
      let user = fixture()
      let token;
      User.create(user)
        .then(data => {
          let { _id } = data
          token = jwt.sign({ _id }, process.env.VERIFICATION_SECRET)
          return User.findByIdAndDelete(_id)
        })
        .then(data => {        
          request(server)
            .get('/api/v1/auth/verify?verification_token=' + token)
            .expect(307)
            .end((err, res) => {
              expect(res.text.includes("success=false")).toEqual(true)
              expect(res.text.includes("message=User%20not%20found!")).toEqual(true)
              done()
            })
        })
    })
  })

  describe('GET /api/v1/auth/me', () => {
    let init = fixture()
    
    beforeAll(done => {
      User.create(init)
        .then(() => {
          delete init.password_confirmation
          done()
        })
    })

    afterAll(done => {
      User.remove()
        .then(() => done())
    })
    
    test("Return current user's information", done => {
      User.authenticate(init)
        .then(data => {
          request(server)
            .get('/api/v1/auth/me')
            .set('Authorization', `Bearer ${data.token}`)
            .expect(200)
            .end((err, res) => {
              expect(res.body.status).toEqual(true)
              expect(res.body.data).toHaveProperty('_id')
              expect(res.body.data).toHaveProperty('email')
              expect(res.body.data).toHaveProperty('username')
              expect(res.body.data).toHaveProperty('isVerified')
              expect(res.body.data).not.toHaveProperty('encryptedPassword')
              done()
            })
        })
    })

    test("Return current user's information with detail", done => {
      let user;
      User.authenticate(init)
        .then(data => {
          user = data;
          return User.verify(data._id)
        })
        .then(data => { 
          request(server)
            .get('/api/v1/auth/me')
            .set('Authorization', `Bearer ${user.token}`)
            .expect(200)
            .end((err, res) => {
              expect(res.body.status).toEqual(true)
              expect(res.body.data).toHaveProperty('_id')
              expect(res.body.data).toHaveProperty('email')
              expect(res.body.data).toHaveProperty('username')
              expect(res.body.data).toHaveProperty('isVerified')
              expect(res.body.data).toHaveProperty('detail')
              expect(res.body.data).not.toHaveProperty('encryptedPassword')
              done()
            })
        })
    })

    test("Won't return user information due to no Bearer prefix", done => {
      User.authenticate(init)
        .then(data => {
          request(server)
            .get('/api/v1/auth/me')
            .set('Authorization', `${data.token}`)
            .expect(401)
            .end((err, res) => {
              expect(res.body.status).toEqual(false)
              expect(res.body.errors).toBe('Token should include bearer prefix')
              done()
            })
        })
    })

    test("Won't return user information due to malform token", done => {
      User.authenticate(init)
        .then(data => {
          request(server)
            .get('/api/v1/auth/me')
            .set('Authorization', `Bearer ${data.token.slice(1)}`)
            .expect(401)
            .end((err, res) => {
              expect(res.body.status).toEqual(false)
              expect(res.body.errors).toBe('Invalid token')
              done()
            })
        })
    })
  })

  describe('POST /api/v1/auth/forgot-password', () => {
    let init = fixture()
    
    beforeAll(done => {
      User.create(init)
        .then(() => {
          delete init.password_confirmation
          done()
        })
    })

    afterAll(done => {
      User.remove()
        .then(() => done())
    })
    
    test("Successfully sent request to reset password", done => {
      let body = {
        email: init.email
      }

      User.findOneAndUpdate({ email: init.email }, { isVerified: true })
        .then(() => {
          body = JSON.stringify(body)

          request(server)
            .post('/api/v1/auth/forgot-password')
            .set('Content-Type', 'application/json')
            .send(body)
            .expect(201)
            .end((err, res) => {
              expect(res.body.status).toEqual(true)
              expect(res.body.data).toBe('Your request has sent, please check your email to reset your password!')
              done()
            })
        
        })
    })

    test("Failed to sent reset password request because of email isn't verified yet", done => {
      let body = {
        email: init.email
      }

      User.findOneAndUpdate({ email: init.email }, { isVerified: false })
        .then(() => {
          body = JSON.stringify(body)

          request(server)
            .post('/api/v1/auth/forgot-password')
            .set('Content-Type', 'application/json')
            .send(body)
            .expect(400)
            .end((err, res) => {
              expect(res.body.status).toEqual(false)
              expect(res.body.errors).toBe('Please verify your email!')
              done()
            })
        
        })
    })

    test("Failed to sent reset password request, because of user not found", done => {
      let body = {
        email: fixture().email
      }

      body = JSON.stringify(body)

      request(server)
        .post('/api/v1/auth/forgot-password')
        .set('Content-Type', 'application/json')
        .send(body)
        .expect(400)
        .end((err, res) => {
          expect(res.body.status).toEqual(false)
          expect(res.body.errors).toBe('User not found!')
          done()
        })
    })
  })

  describe('POST /api/v1/auth/reset-password', () => {
    let init = fixture()
    let password;
    
    beforeAll(done => {
      User.create(init)
        .then(({ _id }) => {
          delete init.password_confirmation
          return User.findByIdAndUpdate(_id, {
            isVerified: true
          })
        })
        .then(() => done())
    })

    afterAll(done => {
      User.remove()
        .then(() => done())
    })
    
    test("Successfully reset password", done => {
      User.generateForgotPasswordToken(init.email)
        .then(token => {
          let newPassword = fixture().password
          password = newPassword;

          let body = {
            password: newPassword,
            password_confirmation: newPassword
          }
          body = JSON.stringify(body)

          request(server)
            .post('/api/v1/auth/reset-password?token=' + token)
            .set('Content-Type', 'application/json')
            .send(body)
            .expect(201)
            .end((err, res) => {
              expect(res.body.data).toBe('Password has changed!')
              /* Check if the new password is a valid password */
              User.authenticate({
                email: init.email,
                password: newPassword
              })
                .then(data => {
                  expect(data).toHaveProperty('token')
                  done()
                })
            }) 
        })
    })

    test("Won't change password because password confirmation doesn't match", done => {
      User.generateForgotPasswordToken(init.email)
        .then(token => {
          let newPassword = fixture().password
          password = newPassword;

          let body = {
            password: newPassword,
            password_confirmation: fixture().password
          }
          body = JSON.stringify(body)

          request(server)
            .post('/api/v1/auth/reset-password?token=' + token)
            .set('Content-Type', 'application/json')
            .send(body)
            .expect(400)
            .end((err, res) => {
              expect(res.body.errors).toBe("Password doesn't match")
              done()
            }) 
        })
    })

    test("Won't change password because of incomplete properties", done => {
      User.generateForgotPasswordToken(init.email)
        .then(token => {
          let newPassword = fixture().password
          password = newPassword;

          let body = {
            password: newPassword,
          }
          body = JSON.stringify(body)

          request(server)
            .post('/api/v1/auth/reset-password?token=' + token)
            .set('Content-Type', 'application/json')
            .send(body)
            .expect(400)
            .end((err, res) => {
              expect(res.body.errors).toBe("password or password_confirmation is required")
              done()
            }) 
        })
    })

    test("Won't change password because of invalid token", done => {
      User.generateForgotPasswordToken(init.email)
        .then(token => {
          let newPassword = fixture().password
          password = newPassword;

          let body = {
            password: newPassword,
          }
          body = JSON.stringify(body)

          request(server)
            .post('/api/v1/auth/reset-password?token=' + token + faker.lorem.words().trim())
            .set('Content-Type', 'application/json')
            .send(body)
            .expect(400)
            .end((err, res) => {
              expect(res.body.errors).toBe('invalid token')
              done()
            }) 
        })
    })

    test("Won't change password because there's no token passed", done => {
      let newPassword = fixture().password
      password = newPassword;

      let body = {
        password: newPassword,
      }
      body = JSON.stringify(body)

      request(server)
        .post('/api/v1/auth/reset-password?token=')
        .set('Content-Type', 'application/json')
        .send(body)
        .expect(400)
        .end((err, res) => {
          expect(res.body.errors).toBe('jwt must be provided')
          done()
        }) 
    })

  })
})
