const faker = require('faker')

module.exports = () => {
  let password = faker.internet.password()

  return {
    username: faker.internet.userName(),
    email: faker.internet.email(),
    password: password,
    password_confirmation: password
  }
}
